%   MAIN_SC In this file, we compare the performance of the proposed
%   suboptimal stopping criteria for selecting the number of transmit
%   antennas.

%   Other m-files required: permn.m 
%                           channelgain_Winner.m

%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   This file is the code used for the simulations done in the paper:
%   Bonnefoi, R.; Moy, C.; Palicot, J.; Nafkha A. � Low-Complexity Antenna 
%   Selection for Minimizing the Power Consumption of a MIMO Base Station �,
%   AICT, July 2018. 

%   Author  : R�mi BONNEFOI
%   SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 08/05/2018

clear all;
close all;
clc;

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

% Simulation parameters
Nb_iter         = 1000; 

% Base station parameters
% Bandwidth

B               = 1*10^6;
N_t             = 32;

P_RF_chain      = 120;
m               = 1/0.35;
Pmax            = 20000;
eps             = 0.05;

% User
N_r             = 8;

% Coefficient multiplied by the power in order to compute the SNR
% SNR_max_dB      = 0:1:30;
% SNR_max         = 10.^(SNR_max_dB/10); % SNR that would have the user if he was served with the maximum transmit power.
% 
% C_snr           = SNR_max/Pmax;
d               = 50:50:1000;
% pathloss parameters
frequency   = 2*10^9;
Wmodel      = 'C1';

Cap_const       = 20*10^6;  % User capacity constraint

% For NBS approach
s_min_pow_NBS       = zeros(1,length(d));
s_Nb_ant_NBS        = zeros(1,length(d));
s_tr_pow_NBS        = zeros(1,length(d));

% Without antenna selection
s_min_pow_nosel     = zeros(1,length(d));
s_tr_pow_nosel      = zeros(1,length(d));

% With a suboptimal evaluation criteria
s_min_pow_apr       = zeros(1,length(d));
s_Nb_ant_apr        = zeros(1,length(d));
s_tr_pow_apr        = zeros(1,length(d));

% Solution proposed by Xu11
s_min_pow_Xu        = zeros(1,length(d));
s_Nb_ant_Xu         = zeros(1,length(d));
s_tr_pow_Xu         = zeros(1,length(d));

% Number of good iterations
Nb_ok               = zeros(1,length(d));

test                = 0;
test2               = 0;
parfor ind_d = 1:1:length(d)
    for ind_iter =1:1:Nb_iter
        
        % Computation of the channel gain
        [PL]     = channelgain_Winner(Wmodel,d(1,ind_d),frequency);
        N0          = -174;
        
        NF          = 2;       
        Noise       = N0+NF+10*log10(B);
        C_snr_dB    = PL+Noise;
        C_snr       = 10^(-C_snr_dB/10);
         
        
        % Generate the channel matrix H
        H           = normrnd(0,1/sqrt(2),N_r,N_t)+1i*normrnd(0,1/sqrt(2),N_r,N_t);
         
        %%%%%%%%%%%%%%%%%%%%%%%%
        % NORM BASED SELECTION %
        %%%%%%%%%%%%%%%%%%%%%%%%
        % With this policy, the line and column of the channel matrix
        % which has are removed considering only the diagonal element of
        % the matrix
        
        Pow_cons_NBS   = zeros(1,N_t);
        
        % Reorder the line and columns of the channel matrix
        norm_row_H          = zeros(1,N_t);
        
        for i =1:1:N_t
            norm_row_H(1,i)             = norm(H(:,i));
        end
        [~, ind ]          = sort(norm_row_H);
        
        H_2                = H(:,ind); 
        H_temp             = H_2;
        % Water filling algorithm
        for i =1:1:N_t
            [~,S,~]     = svd(H_temp);
            lambda      = diag(S);
            lambda      = lambda.^2;
            lambda      = lambda(lambda>0)';

            % Water filling algorithm
            Ptx         = -1*ones(1,length(lambda));
            lambda_temp = lambda;

            while(min(Ptx)<0)
                N_no_zero   = length(lambda_temp);
                Geom        = prod(lambda_temp)^(1/N_no_zero);
                Ptx         = (2^(Cap_const/(N_no_zero*B))/(C_snr*Geom))-1./((C_snr*lambda_temp));
                lambda_temp = lambda_temp(Ptx>0);
            end
            
            if sum(Ptx)<Pmax,
                Pow_cons_NBS(1,i)   = (N_t-i+1)*P_RF_chain+m*sum(Ptx);
            else
                Pow_cons_NBS(1,i)   = Inf(1);
            end

            H_temp              = H_temp(:,2:size(H_temp,2));
        end
        
            [M,I]                         = min(Pow_cons_NBS);
            
        if (Pow_cons_NBS==Inf(1,length(Pow_cons_NBS))),
            test            = test+1;
        else
            Nb_ok(1,ind_d)                = Nb_ok(1,ind_d)+1;
            s_tr_pow_NBS(1,ind_d)         = s_tr_pow_NBS(1,ind_d)+(M-P_RF_chain*(N_t-I+1))/m;
            s_min_pow_NBS(1,ind_d)        = s_min_pow_NBS(1,ind_d) + M;
            s_Nb_ant_NBS(1,ind_d)         = N_t-I+1+s_Nb_ant_NBS(1,ind_d);
        
           % Without antenna selection
           s_min_pow_nosel(1,ind_d)       = s_min_pow_nosel(1,ind_d) + Pow_cons_NBS(1,1);
           s_tr_pow_nosel(1,ind_d)        = s_tr_pow_nosel(1,ind_d)  + (Pow_cons_NBS(1,1)-P_RF_chain*(N_t))/m ;
        
        
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Suboptimal selection of the number of antennas %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            % Computation of the evaluation criteria at each iteration
            Eval_power_cons                     = zeros(1,N_t);

            H_2_hermitian                       = H_2'*H_2;

            for i = 1:1:N_t
                Sum_vp                          = sum(diag(H_2_hermitian(N_t-i+1:N_t,N_t-i+1:N_t)));
                % Evaluation of the coeffcient
                N                               = min(N_r,i);
                Value2                          = (1/C_snr)*(1/Sum_vp)*N^2*(2^(Cap_const/(N*B))-1);
                if Value2 <Pmax*(1-eps)
                    Eval_power_cons(1,i)            = i*P_RF_chain+m*Value2;
                else
                    Eval_power_cons(1,i)            = Inf(1);
                end
            end

            if (Eval_power_cons==Inf(1,length(Eval_power_cons))),
                test2            = test2+1;
            end

            % Finding the minimal value
            [~,I]                   = min(Eval_power_cons);
            H_temp                  = H_2(:,N_t-I+1:N_t);
            [U,S,V]                 = svd(H_temp);
            lambda                  = diag(S);
            lambda                  = lambda.^2;
            lambda                  = lambda(lambda>0)';

            Ptx         = -1*ones(1,length(lambda));
            lambda_temp = lambda;

            while(min(Ptx)<0)
                N_no_zero   = length(lambda_temp);
                Geom        = prod(lambda_temp)^(1/N_no_zero);
                Ptx         = (2^(Cap_const/(N_no_zero*B))/(C_snr*Geom))-1./((C_snr*lambda_temp));
                lambda_temp = lambda_temp(Ptx>0);
            end

            % Save results
            s_tr_pow_apr(1,ind_d)         = s_tr_pow_apr(1,ind_d)+sum(Ptx);
            s_min_pow_apr(1,ind_d)        = s_min_pow_apr(1,ind_d) + I*P_RF_chain+m*sum(Ptx);
            s_Nb_ant_apr(1,ind_d)         = I+s_Nb_ant_apr(1,ind_d);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Another solution [Xu11]  %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Z. Xu, C. Yang, G. Y. Li, S. Zhang, Y. Chen and S. Xu, 
            % "Energy-Efficient MIMO-OFDMA Systems Based on Switching off 
            % RF Chains," 2011 IEEE Vehicular Technology Conference 
            % (VTC Fall), San Francisco, CA, 2011, pp. 1-5.

            % Number of transmit antennas

            Nb_ant                  = sqrt((m*N_r*(2^(Cap_const/(N_r*B))-1))/(P_RF_chain*C_snr));
            %Nb_ant2                 = sqrt(P_RF_chain*C_snr/(m*N_r*(2^(Cap_const/(N_r*B))-1)));
            if Nb_ant<N_r,
                Nb_ant              = N_r;
            end
            if Nb_ant>N_t,
                Nb_ant              = N_t;
            end

            Nb_ant                  = round(Nb_ant);

            % Matrix size
            H_Xu                    = H(:,N_t-Nb_ant+1:N_t);
            H_Xu_herm               = H_Xu'*H_Xu;

            % Transmit power with Water-filling
            [U,S,V]                 = svd(H_Xu);
            lambda                  = diag(S);
            lambda                  = lambda.^2;
            lambda                  = lambda(lambda>0)';

            Ptx         = -1*ones(1,length(lambda));
            lambda_temp = lambda;

            while(min(Ptx)<0)
                N_no_zero   = length(lambda_temp);
                Geom        = prod(lambda_temp)^(1/N_no_zero);
                Ptx         = (2^(Cap_const/(N_no_zero*B))/(C_snr*Geom))-1./((C_snr*lambda_temp));
                lambda_temp = lambda_temp(Ptx>0);
            end

            % Get results
            s_min_pow_Xu(1,ind_d)        = s_min_pow_Xu(1,ind_d) + sum(Ptx)*m+Nb_ant*P_RF_chain;
            s_Nb_ant_Xu(1,ind_d)         = s_Nb_ant_Xu(1,ind_d)+ Nb_ant;
            s_tr_pow_Xu(1,ind_d)         = s_tr_pow_Xu(1,ind_d) + sum(Ptx);
        end
    end
end

av_min_pow              = s_min_pow_apr/Nb_iter;
av_Nb_ant               = s_Nb_ant_apr/Nb_iter;
av_tr_pow               = s_tr_pow_apr/Nb_iter;

%av_tr_pow_all           = s_tr_pow_all/Nb_iter;

% NBS based
av_min_pow_NBS              = s_min_pow_NBS/Nb_iter;
av_Nb_ant_NBS               = s_Nb_ant_NBS/Nb_iter;
av_tr_pow_NBS               = s_tr_pow_NBS/Nb_iter;

% Without antenna selection
av_min_pow_nosel            = s_min_pow_nosel/Nb_iter;
av_tr_pow_nosel             = s_tr_pow_nosel/Nb_iter;

% Xu11 solution
av_min_pow_Xu               = s_min_pow_Xu /Nb_iter;
av_Nb_ant_Xu                = s_Nb_ant_Xu /Nb_iter;
av_tr_pow_Xu                = s_tr_pow_Xu /Nb_iter;

figure;
grid on;
box on;
hold on;
plot(d,av_min_pow_NBS/1000,'-');
plot(d,av_min_pow/1000,'*');
plot(d,av_min_pow_nosel/1000,'-v');
plot(d,av_min_pow_Xu/1000,'-s','Color',[0.4940 0.1840 0.5560]);
ylabel('Minimum power consumption (W)');
xlabel('distance (m)');
legend('Iterative','Suboptimal criteria','W/o switching off RF chains','Antenna selection from [8]');
xlim([50 1000]);
set(gca,'FontSize',13)

figure;
grid on;
box on;
hold on;
plot(d,av_Nb_ant_NBS,'-');
plot(d,av_Nb_ant,'*');
plot(d,av_Nb_ant_Xu,'-s','Color',[0.4940 0.1840 0.5560]);
ylabel('Average number of antennas');
xlabel('distance (m)');
legend('Iterative','Suboptimal criteria','Antenna selection from [8]');
xlim([50 1000]);
set(gca,'FontSize',13)

figure;
grid on;
box on;
hold on;
plot(d,av_tr_pow_NBS/1000,'-');
plot(d,av_tr_pow/1000,'*');
plot(d,av_tr_pow_nosel/1000,'-v');
plot(d,av_tr_pow_Xu/1000,'-s','Color',[0.4940 0.1840 0.5560]);
ylabel('Average transmit power (W)');
xlabel('distance (m)');
legend('Iterative','Suboptimal criteria','W/o switching off RF chains','Antenna selection from [8]');
xlim([50 1000]);
set(gca,'FontSize',13)