%   MAIN_NBS Comparison of the Norm-Based Selection with an exhaustive
%   search. This file shows that both approaches provide similar
%   performances

%   Other m-files required: permn.m 
%                           channelgain_Winner.m

%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   This file is the code used for the simulations done in the paper:
%   Bonnefoi, R.; Moy, C.; Palicot, J.; Nafkha A. � Low-Complexity Antenna 
%   Selection for Minimizing the Power Consumption of a MIMO Base Station �,
%   AICT, July 2018. 

%   Author  : R�mi BONNEFOI
%   SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 08/05/2018

clear all;
close all;
clc;

%%%%%%%%%%%%%%
% PARAMETERS %
%%%%%%%%%%%%%%

% Simulation parameters
Nb_iter         = 1000; 

% Base station parameters
% Bandwidth

B               = 1*10^6;
N_t             = 12;
% Power parameters
P_RF_chain      = 120;
m               = 1/0.35;
Pmax            = 20000;

% User
N_r             = 4;

% Coefficient multiplied by the power in order to compute the SNR

% distance

d               = 50:50:1000;

% pathloss parameters
frequency   = 2*10^9;
Wmodel      = 'C1';

Cap_const       = 5*10^6;  % User capacity constraint in bits/s/Hz

% Possible antennas configurations
conf            = permn([0,1],N_t);
conf            = conf(2:size(conf,1),:);
s_min_pow       = zeros(1,length(d));
s_Nb_ant        = zeros(1,length(d));
s_tr_pow        = zeros(1,length(d));
s_tr_pow_all    = zeros(length(conf),length(d));


% For NBS approach
s_min_pow_NBS   = zeros(1,length(d));
s_Nb_ant_NBS    = zeros(1,length(d));
s_tr_pow_NBS    = zeros(1,length(d));

for ind_iter =1:1:Nb_iter
    for ind_dist = 1:1:length(d)
        
        % Computation of the channel gain
        [PL]     = channelgain_Winner(Wmodel,d(1,ind_dist),frequency);
        N0          = -174;
        
        NF          = 2;       
        Noise       = N0+NF+10*log10(B);
        C_snr_dB    = PL+Noise;
        C_snr       = 10^(-C_snr_dB/10);
        
        % Generate the channel matrix H
        H           = normrnd(0,1/sqrt(2),N_r,N_t)+1i*normrnd(0,1/sqrt(2),N_r,N_t);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % OPTIMAL POLICY (Iterative search) %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Switching off antennas
        % perf_conf       = zeros(N_r,length(conf));
        Pow_cons_conf   = zeros(1,length(conf));
        for i = 1:1:length(conf)

            H_temp              = H(:,conf(i,:)==1);

            [U,S,V]     = svd(H_temp);
            lambda      = diag(S);
            lambda      = lambda.^2;
            lambda      = lambda(lambda>0)';

            % Water filling algorithm
            Ptx         = -1*ones(1,length(lambda));
            lambda_temp = lambda;

            while(min(Ptx)<0)
                N_no_zero   = length(lambda_temp);
                Geom        = prod(lambda_temp)^(1/N_no_zero);
                Ptx         = (2^(Cap_const/(N_no_zero*B))/(C_snr*Geom))-1./((C_snr*lambda_temp));
                lambda_temp = lambda_temp(Ptx>0);
            end

            % Computing results
            Pow_cons_conf(1,i)   = sum(conf(i,:)==1)*P_RF_chain+m*sum(Ptx);
            
            
        end
            
            s_tr_pow_all(:,ind_dist)     = (Pow_cons_conf'-sum(conf,2))/m;
            [M,I]                        = min(Pow_cons_conf);
            s_tr_pow(1,ind_dist)         = s_tr_pow(1,ind_dist)+(M-P_RF_chain*sum(conf(I,:)))/m;
            s_min_pow(1,ind_dist)        = s_min_pow(1,ind_dist) + M;
            s_Nb_ant(1,ind_dist)         = sum(conf(I,:))+s_Nb_ant(1,ind_dist);
            
            
        %%%%%%%%%%%%%%%%%%%%%%%%
        % NORM BASED SELECTION %
        %%%%%%%%%%%%%%%%%%%%%%%%
        % With this policy, the line and column of the channel matrix
        % which has are removed considering only the diagonal element of
        % the matrix
        
        Pow_cons_NBS   = zeros(1,N_t);
        
        % Reorder the line and columns of the channel matrix
        norm_row_H          = zeros(1,N_t);
        
        for i =1:1:N_t
            norm_row_H(1,i)             = norm(H(:,i));
        end
        [~, ind ]          = sort(norm_row_H);
        
        H_2                = H(:,ind);
            
        H_temp             = H_2;
        % Water filling algorithm
        for i =1:1:N_t
            [U,S,V]     = svd(H_temp);
            lambda      = diag(S);
            lambda      = lambda.^2;
            lambda      = lambda(lambda>0)';

            % Water filling algorithm
            Ptx         = -1*ones(1,length(lambda));
            lambda_temp = lambda;

            while(min(Ptx)<0)
                N_no_zero   = length(lambda_temp);
                Geom        = prod(lambda_temp)^(1/N_no_zero);
                Ptx         = (2^(Cap_const/(N_no_zero*B))/(C_snr*Geom))-1./((C_snr*lambda_temp));
                lambda_temp = lambda_temp(Ptx>0);
            end

            % Computing results
            Pow_cons_NBS(1,i)   = (N_t-i+1)*P_RF_chain+m*sum(Ptx);
            H_temp              = H_temp(:,2:size(H_temp,2));
        end
        
            [M,I]                           = min(Pow_cons_NBS);
            s_tr_pow_NBS(1,ind_dist)         = s_tr_pow_NBS(1,ind_dist)+(M-P_RF_chain*(N_t-I+1))/m;
            s_min_pow_NBS(1,ind_dist)        = s_min_pow_NBS(1,ind_dist) + M;
            s_Nb_ant_NBS(1,ind_dist)         = N_t-I+1+s_Nb_ant_NBS(1,ind_dist);
            
    end
end

av_min_pow              = s_min_pow/Nb_iter;
av_Nb_ant               = s_Nb_ant/Nb_iter;
av_tr_pow               = s_tr_pow/Nb_iter;

av_tr_pow_all           = s_tr_pow_all/Nb_iter;

% NBS based
av_min_pow_NBS              = s_min_pow_NBS/Nb_iter;
av_Nb_ant_NBS               = s_Nb_ant_NBS/Nb_iter;
av_tr_pow_NBS               = s_tr_pow_NBS/Nb_iter;

figure;
grid on;
box on;
hold on;
plot(d,av_min_pow/1000,'-');
plot(d,av_min_pow_NBS/1000,'*');
ylabel('Minimum power consumption (W)');
xlabel('SNR (dB)');
legend('Iterative search','Norm Based Selection');

figure;
grid on;
box on;
hold on;
plot(d,av_Nb_ant,'-');
plot(d,av_Nb_ant_NBS,'*');
ylabel('Average number of antennas');
xlabel('SNR (dB)');
legend('Iterative search','Norm Based Selection');

figure;
grid on;
box on;
hold on;
plot(d,av_tr_pow/1000,'-');
plot(d,av_tr_pow_NBS/1000,'*');
ylabel('Average transmit power (W)');
xlabel('SNR (dB)');
legend('Iterative search','Norm Based Selection');

figure;
grid on;
box on;
plot(av_Nb_ant,av_tr_pow/1000,'-+');
ylabel('Average transmit power (W)');
xlabel('Number of switched on antennas');


% figure;
% plot(av_tr_pow_all')